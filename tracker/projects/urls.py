from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="list_projects"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
