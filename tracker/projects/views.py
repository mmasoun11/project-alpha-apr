from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .models import Project, ProjectForm


# Create your views here.
@login_required
def index(request):
    projects = {"projects": Project.objects.filter(owner=request.user)}
    return render(request, "projects/index.html", projects)


@login_required
def show_project(request, id):
    data = get_object_or_404(Project, id=id, owner=request.user)
    project = {"project": data}
    return render(request, "projects/tasks.html", project)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/project.html", {"form": form})
