from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Task, TaskForm

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.is_completed = False
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/index.html", {"form": form})


@login_required
def my_tasks(request):
    tasks = {"tasks": Task.objects.filter(assignee=request.user)}
    return render(request, "tasks/tasks.html", tasks)
